from decimal import Decimal

from rest_framework import viewsets, permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import *
from .models import *


def is_null_decimal(var):
    try:
        return Decimal(var)
    except:
        return False


class SubjectViewSet(viewsets.ModelViewSet):
    queryset = Subject.objects.all().order_by('-pk')
    serializer_class = SubjectSerializer

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        if self.action in ['create', 'update', 'partial_update', 'destroy']:
            permission_classes = [permissions.IsAuthenticated]
        else:
            permission_classes = []
        return [permission() for permission in permission_classes]


class ArticleViewSet(viewsets.ModelViewSet):
    queryset = Article.objects.all().order_by('-pk')
    serializer_class = ArticleSerializer

    def get_queryset(self):
        queryset = self.queryset
        homepage = self.request.GET.get('homepage', False)
        # print(homepage)
        last = self.request.GET.get('last', False)
        if homepage == 'true':
            queryset = Article.objects.filter(homepage=True).order_by('-pk')[:1]

        if last == 'true':
            queryset = Article.objects.filter(homepage=False).order_by('-pk')[:2]
        return queryset

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        if self.action in ['create', 'update', 'partial_update', 'destroy']:
            permission_classes = [permissions.IsAuthenticated]
        else:
            permission_classes = []
        return [permission() for permission in permission_classes]


class ExamViewSet(viewsets.ModelViewSet):
    queryset = Exam.objects.all().order_by('-pk')
    serializer_class = ExamSerializer

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        if self.action in ['create', 'update', 'partial_update', 'destroy']:
            permission_classes = [permissions.IsAuthenticated]
        else:
            permission_classes = []
        return [permission() for permission in permission_classes]


class SubjectSearchViewSet(APIView):
    # queryset = Subject.objects.all().order_by('-pk')
    serializer_class = SubjectSearchSerializer

    def get(self, request, format=None):
        subject = self.request.GET.get('subject', False)
        if is_null_decimal(subject):
            queryset = Exam.objects.filter(subject=subject).order_by('-pk')
            sd = SubjectSearchSerializer(queryset, many=True)
        return Response(sd.data)
