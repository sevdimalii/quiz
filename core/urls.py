from django.urls import include, path
from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'subjects', views.SubjectViewSet, basename='subjects')
router.register(r'exams', views.ExamViewSet, basename='exams')
router.register(r'articles', views.ArticleViewSet, basename='articles')
# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('search/', views.SubjectSearchViewSet.as_view(), name='search_subject'),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]