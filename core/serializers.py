from rest_framework import serializers
from drf_extra_fields.fields import Base64ImageField
from .models import *


class ArticleSerializer(serializers.ModelSerializer):
    featured_img = Base64ImageField()
    def get_featured_img(self, obj):
        return self.context['request'].build_absolute_uri(obj.featured_img)

    class Meta:
        model = Article
        fields = ('__all__')


class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = ('__all__')


class ExamSerializer(serializers.ModelSerializer):
    subject = SubjectSerializer(many=True)

    class Meta:
        model = Exam
        fields = ('__all__')
        depth = 2


class SubjectSearchSerializer(serializers.ModelSerializer):
    subject = SubjectSerializer(many=True)

    class Meta:
        model = Exam
        fields = ('__all__')
        depth = 2
