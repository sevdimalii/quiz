from django.contrib import admin
from .models import *

admin.site.register(Subject)
admin.site.register(Exam)
admin.site.register(Article)