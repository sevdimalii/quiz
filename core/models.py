from django.db import models


# Create your models here.


class Subject(models.Model):
    title = models.CharField(null=True, blank=True, max_length=200)
    slug = models.SlugField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class Exam(models.Model):
    title = models.CharField(null=True, blank=True, max_length=200)
    subject = models.ManyToManyField(Subject)
    google_doc_url = models.TextField(null=True, blank=True)
    is_active = models.BooleanField(default=False)
    slug = models.SlugField(max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class Article(models.Model):
    title = models.CharField(null=True, blank=True, max_length=200)
    content = models.TextField(null=True, blank=True)
    featured_img = models.ImageField(upload_to='uploads/', verbose_name="Məqalə şəkli")
    homepage = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField(max_length=200)

    def __str__(self):
        return self.title